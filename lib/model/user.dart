class User {
  User({
    this.id,
    this.rut,
    this.email,
    this.nombres,
    this.genero,
    this.emailVerifiedAt,
    this.createdAt,
    this.updatedAt,
    this.comisaria_id,
  });

  int id;
  String rut;
  String email;
  String nombres;
  String genero;
  DateTime emailVerifiedAt;
  DateTime createdAt;
  DateTime updatedAt;
  int comisaria_id;

  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json["id"],
    rut: json["rut"],
    email: json["email"],
    nombres: json["nombres"],
    genero: json["genero"],
    emailVerifiedAt: DateTime.parse(json["email_verified_at"]),
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    comisaria_id: json["comisaria_id"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "rut": rut,
    "email": email,
    "nombres": nombres,
    "genero": genero,
    "email_verified_at": emailVerifiedAt.toIso8601String(),
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "comisaria_id": comisaria_id.toString(),
  };
}
