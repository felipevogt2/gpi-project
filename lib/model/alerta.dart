import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Alerta {
  Alerta({
    this.id_alerta,
    this.descripcion,
    this.latitud,
    this.longitud,
    this.rut_alertante,
    this.nombre_victima,
    this.nombre_atacante,
    this.tipo_victima,
    this.vinculo_atacante,
    this.asignada,
    this.comisaria_id,
    this.id_alerta_funcionario,
    this.id_tipo_alerta,
    this.nombre_alerta,
    this.priorizacion,
    this.id_categoria,
    this.nombre_categoria,
    this.color,
    this.imagen,
    this.nombre,
    this.createdAt,
    this.user_id,
  });

  int id_alerta;
  String descripcion;
  String latitud;
  String longitud;
  String rut_alertante;
  String nombre_victima;
  String nombre_atacante;
  String tipo_victima;
  String vinculo_atacante;
  String asignada;
  int comisaria_id;
  int id_alerta_funcionario;
  int id_tipo_alerta;
  String nombre_alerta;
  String priorizacion;
  int id_categoria;
  String nombre_categoria;
  String color;
  String imagen;
  String nombre;
  DateTime createdAt;
  int user_id;

  factory Alerta.fromJson(Map<String, dynamic> json) => Alerta(
        id_alerta: json["id_alerta"],
        descripcion: json["descripcion"],
        latitud: json["latitud"],
        longitud: json["longitud"],
        rut_alertante: json["rut_alertante"],
        nombre_victima: json["nombre_victima"],
        nombre_atacante: json["nombre_atacante"],
        tipo_victima: json["tipo_victima"],
        vinculo_atacante: json["vinculo_atacante"],
        asignada: json["asignada"],
        comisaria_id: json["comisaria_id"],
        id_alerta_funcionario: json["id_alerta_funcionario"],
        id_tipo_alerta: json["id_tipo_alerta"],
        nombre_alerta: json["nombre_alerta"],
        priorizacion: json["priorizacion"],
        id_categoria: json["id_categoria"],
        nombre_categoria: json["nombre_categoria"],
        color: json["color"],
        imagen: json["imagen"],
        nombre: json["nombre"],
        user_id: json["user_id"],
        createdAt: DateTime.parse(json["created_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id_alerta": id_alerta.toString(),
        "descripcion": descripcion,
        "latitud": latitud,
        "longitud": longitud,
        "rut_alertante": rut_alertante,
        "nombre_victima": nombre_victima,
        "nombre_atacante": nombre_atacante,
        "tipo_victima": tipo_victima,
        "vinculo_atacante": vinculo_atacante,
        "asignada": asignada,
        "comisaria_id": comisaria_id.toString(),
        "id_alerta_funcionario": id_alerta_funcionario.toString(),
        "id_tipo_alerta": id_tipo_alerta.toString(),
        "nombre_alerta": nombre_alerta,
        "priorizacion": priorizacion,
        "id_categoria": id_categoria.toString(),
        "nombre_categoria": nombre_categoria,
        "color": color,
        "imagen": imagen,
        "nombre": nombre,
        "user_id": user_id,
        "created_at": createdAt.toIso8601String(),
      };
}
