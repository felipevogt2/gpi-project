import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smart_select/smart_select.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart';

class FormularioAlerta extends StatefulWidget {
  final int id_categoria;

  FormularioAlerta(this.id_categoria);

  @override
  State<StatefulWidget> createState() => FormularioAlertaState();
}

class FormularioAlertaState extends State<FormularioAlerta> {
  String descripcion;
  String rut_alertante;
  String nombre_victima;
  String nombre_atacante;
  String tipo_atacante;
  String tipo_victima;
  String vinculo_victima;
  String direccion;
  String latitud;
  String longitud;
  var txt = TextEditingController();
  Location location = new Location();
  bool input = false;
  bool input2 = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  List<S2Choice<String>> optionsVictima = [
    S2Choice<String>(value: 'Yo', title: 'Yo'),
    S2Choice<String>(value: 'Madre', title: 'Madre'),
    S2Choice<String>(value: 'Hijo', title: 'Hijo'),
  ];

  List<S2Choice<String>> optionsAtacante = [
    S2Choice<String>(value: 'Yo', title: 'Yo'),
    S2Choice<String>(value: 'Madre', title: 'Madre'),
    S2Choice<String>(value: 'Hijo', title: 'Hijo'),
  ];

  List<S2Choice<String>> optionsMaltrato = [];
  String tipo_alerta;

  @override
  void initState() {
    super.initState();
    getUserData();
    getTipoAlertas();
  }
  _showMsg(msg) {
    final snackBar = SnackBar(
      content: Text(msg),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {
          // Some code to undo the change!
        },
      ),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
  void getTipoAlertas() async {
    try {
      String url =
          'http://34.232.65.48/api/verTiposAlerta/${widget.id_categoria.toString()}';
      var response = await http.get(url);
      var data = jsonDecode(response.body);

      List<S2Choice> options = S2Choice.listFrom<String, dynamic>(
        source: data,
        value: (index, item) => item['id_tipo_alerta'].toString(),
        title: (index, item) => item['nombre_alerta'],
        meta: (index, item) => item,
      );

      setState(() => optionsMaltrato = options);
    } catch (e) {
      print(e);
    } finally {}
  }

  void getUserData() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userData = jsonDecode(localStorage.getString('userData'));
    if (userData != null) {
      setState(() {
        rut_alertante = userData['RolUnico']['numero'].toString() +
            "-" +
            userData['RolUnico']['DV'].toString();
        print(rut_alertante);
      });
    }
  }

  Future<bool> validarDireccion() async {
    print(direccion);
    String url =
        'https://maps.googleapis.com/maps/api/geocode/json?address=${direccion}&components=country:CL&key=AIzaSyDCrbMhvIppUjkm0kO9MF6KoqNia9fJlgc';

    var response = await http.get(url);
    var data = jsonDecode(response.body);

    print(response.body);
    if (data['status'] == 'ZERO_RESULTS') {
      return false;
    } else {
      latitud = data['results'][0]['geometry']['location']['lat'].toString();
      longitud = data['results'][0]['geometry']['location']['lng'].toString();
      print(latitud + " " + longitud);
      return true;
    }
  }

  void sendAlerta() async {
    var valido = false;
    String url = 'http://34.232.65.48/api/agregarAlerta';
    Future.wait<void>([validarDireccion().then((value) => valido = value)])
        .then((value) async {
      if (valido) {
        var response = await http.post(url, headers: {
          'Connection': 'Keep-Alive'
        }, body: {
          'tipo_alerta_id': tipo_alerta,
          'tipo_victima': tipo_victima,
          'nombre_victima': nombre_victima,
          'nombre_atacante': nombre_atacante,
          'vinculo_atacante': tipo_atacante,
          'descripcion': descripcion,
          'rut_alertante': rut_alertante,
          'latitud': latitud,
          'longitud': longitud,
        });
        if (response.statusCode == 200) {
          _showMsg("Alarma enviada");
        }else{
          _showMsg("Ha ocurrido un  prboema...");
        }
        print(response.body);
      }
    });
  }



  void geolocate() async {
    LocationData _location;
    _location = await location.getLocation();

    latitud = _location.latitude.toString();
    longitud = _location.longitude.toString();
    print(latitud + " " + longitud);
    parselocate(latitud, longitud);
  }

  void parselocate(latitud, longitud) async {
    String url =
        'https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitud},${longitud}&key=AIzaSyDCrbMhvIppUjkm0kO9MF6KoqNia9fJlgc';
    var response = await http.get(url);
    var data = jsonDecode(response.body);
    if (response.statusCode == 200) {
      txt.text = data['results'][0]['formatted_address'].toString();
      setState(() {
        direccion = data['results'][0]['formatted_address'].toString();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: const Text('Denuncie un delito'),
        actions: <Widget>[],
        backgroundColor: Color(0xffF67272),
      ),
      body: ListView(
        children: [
          SizedBox(height: 10),
          SmartSelect<String>.single(
              modalType: S2ModalType.bottomSheet,
              title: 'Tipo Delito',
              placeholder: '',
              value: tipo_alerta,
              choiceItems: optionsMaltrato,
              onChange: (state) => setState(() => tipo_alerta = state.value)),
          SmartSelect<String>.single(
              modalType: S2ModalType.bottomSheet,
              title: '¿Quien es la victima?',
              placeholder: '',
              value: tipo_victima,
              choiceItems: optionsVictima,
              onChange: (state) => setState(() => {
                    tipo_victima = state.value,
                    input = true,
                  })),
          Padding(
            padding: EdgeInsets.only(left: 20, right: 20, bottom: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextFormField(
                    enabled: input,
                    decoration: const InputDecoration(
                      hintText: 'Nombre de la victima',
                    ),
                    onChanged: (text) {
                      nombre_victima = text;
                    }),
              ],
            ),
          ),
          SmartSelect<String>.single(
              modalType: S2ModalType.bottomSheet,
              title: '¿Quien es el atacante?',
              placeholder: '',
              value: tipo_atacante,
              choiceItems: optionsAtacante,
              onChange: (state) =>
                  setState(() => {tipo_atacante = state.value, input2 = true})),
          Padding(
            padding: EdgeInsets.only(left: 20, right: 20, bottom: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextFormField(
                    enabled: input2,
                    decoration: const InputDecoration(
                      hintText: 'Nombre del atacante',
                    ),
                    onChanged: (text) {
                      nombre_atacante = text;
                    }),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Describa el suceso"),
                TextFormField(
                    maxLines: 5,
                    decoration: const InputDecoration(
                      hintText: 'Descripcion',
                    ),
                    onChanged: (text) {
                      descripcion = text;
                    }),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.all(20),
            child: Row(
              children: [
                Expanded(
                  flex: 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Direccion"),
                      TextFormField(
                        controller: txt,
                        decoration: const InputDecoration(
                          hintText: 'Direccion',
                        ),
                        onChanged: (text) {
                          direccion = text;
                        },
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 0,
                  child: IconButton(
                    icon: Icon(Icons.gps_fixed),
                    color: Color(0xffF67272),
                    tooltip: 'Localizar',
                    onPressed: () {
                      geolocate();
                    },
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 20),
          Center(
            child: FlatButton(
              color: Color(0xffF67272),
              textColor: Colors.white,
              child: const Text('Enviar'),
              onPressed: () {
                sendAlerta();
              },
            ),
          ),
        ],
      ),
    );
  }
}
