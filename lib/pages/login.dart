import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_app/pages/inicio.dart';
import 'package:http/http.dart' as http;
import 'package:webview_flutter/webview_flutter.dart';
import 'dart:math';
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LoginState();
}

class LoginState extends State<Login> {
  String state;

  @override
  void initState() {
    this.state = this.generateRandomString(20);
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
        backgroundColor: Color(0XFFF67272),
      ),
      body: WebView(
        initialUrl: getAuthenticationUrl(this.state),
        javascriptMode: JavascriptMode.unrestricted,
        onWebViewCreated: (WebViewController webViewController) {},
        onPageStarted: (String url) {
          print(url);
          print(url.contains(
              'https://testing-alertasegura.com/callback/claveunica'));
          if (url.contains(
              'https://testing-alertasegura.com/callback/claveunica')) {
            Uri uri = Uri.parse(url);
            String code = uri.queryParameters['code'];
            String state = uri.queryParameters['state'];
            if (this.state == state) {
              this.login(code, state);
            }
          }
          print(url);
        },
      ),
    );
  }

  Future<void> login(String code, String state) async {
    var url = 'https://accounts.claveunica.gob.cl/openid/token/';
    var response = await http.post(url, body: {
      'client_id': 'ea16f30a8bc44e2e8c6914066a61ff35',
      'client_secret': '143101c155df4fdcbf8b1e1a07b3193a',
      'redirect_uri': 'https://testing-alertasegura.com/callback/claveunica',
      'grant_type': 'authorization_code',
      'code': code,
      'state': state,
    });
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    var data = jsonDecode(response.body);

    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.setString('tokenClaveUnica', json.encode(response.body));

    if (response.statusCode == 200) {
      getUserData(data['access_token']);
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Inicio()),
      );
    }
  }

  Future<void> getUserData(String token) async {
    var url = 'https://accounts.claveunica.gob.cl/openid/userinfo/';
    var response = await http.post(url, headers: {
      'authorization': 'Bearer ' + token,
    });
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.setString('userData', response.body);
  }

  String getAuthenticationUrl(String randomState) {
    print(randomState);
    String clientId = "ea16f30a8bc44e2e8c6914066a61ff35";
    String responseType = "code";
    String scope = "openid run name";
    String redirectUri = "https://testing-alertasegura.com/callback/claveunica";
    String state = randomState;

    String url =
        "https://accounts.claveunica.gob.cl/openid/authorize/?client_id=" +
            clientId +
            "&response_type=" +
            responseType +
            "&scope=" +
            scope +
            "&redirect_uri=" +
            redirectUri +
            "&state=" +
            state;

    return url;
  }

  String generateRandomString(int len) {
    var r = Random();
    const _chars =
        'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    return List.generate(len, (index) => _chars[r.nextInt(_chars.length)])
        .join();
  }
}
