import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/pages/login.dart';
import 'package:flutter_app/pages/login_funcionario.dart';

class PrincipalPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => PrincipalPageState();
}

class PrincipalPageState extends State<PrincipalPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Inicio"),
          backgroundColor: Color(0xffF67272),
        ),
        body: Center(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RaisedButton(
                    onPressed: () =>{
                      Navigator.push(context,MaterialPageRoute(builder: (context) => Login())),
                    },
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    color: Color(0XFFF67272),
                    child: Text('Login Victima',
                        style: TextStyle(fontSize: 20, color: Colors.white)),
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0)),
                ),
                RaisedButton(
                    onPressed: () =>{
                      Navigator.push(context,MaterialPageRoute(builder: (context) => LoginFuncionario())),
                    },
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    color: Color(0XFFF67272),
                    child: Text('Login Funcionario',
                        style: TextStyle(fontSize: 20, color: Colors.white)),
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(10.0))),
              ],
            ),
          ),
        ));
  }
}
