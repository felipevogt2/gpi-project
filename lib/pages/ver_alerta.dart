import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_map_polyline/google_map_polyline.dart';
import 'package:location/location.dart';
import 'package:flutter_app/model/alerta.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:async';
import 'dart:math';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:flutter_app/pages/ver_alerta_detalle.dart';

class VerAlerta extends StatefulWidget {
  VerAlerta({Key key, @required this.alerta}) : super(key: key);
  final Alerta alerta;

  @override
  State<StatefulWidget> createState() => VerAlertaState();
}

class VerAlertaState extends State<VerAlerta> {
  GoogleMapController _controller;
  Location location = new Location();
  LocationData currentLocation;
  StreamSubscription<LocationData> locationSubscription;
  bool isLoading = false;
  String error;
  String direccion = "";
  final Set<Marker> _markers = {};

  LatLng posicionVictima;

  final Set<Polyline> polyline = {};
  List<LatLng> routeCoords = [];
  GoogleMapPolyline googleMapPolyline =
      new GoogleMapPolyline(apiKey: "AIzaSyDCrbMhvIppUjkm0kO9MF6KoqNia9fJlgc");

  @override
  void initState() {
    super.initState();
    posicionVictima = LatLng(double.parse(widget.alerta.latitud),
        double.parse(widget.alerta.longitud));
    cargarLocalizacion();
    locationSubscription =
        location.onLocationChanged().listen((LocationData result) {
      currentLocation = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('Alerta ${widget.alerta.id_alerta}'),
        backgroundColor: Color(0xffF67272),
      ),
      body: Stack(
        children: [
          _googleMap(context),
          _buildContainer(),
        ],
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            backgroundColor: Color(0xffF67272),
            child: Icon(Icons.my_location),
            heroTag: null,
            onPressed: _currentLocation,
          ),
          SizedBox(
            height: 5.0,
          ),
          FloatingActionButton(
            backgroundColor: Color(0xffF67272),
            child: Icon(Icons.zoom_out_map),
            heroTag: null,
            onPressed: _centerView,
          ),
        ],
      ),
    );
  }

  Widget _googleMap(BuildContext context) {
    return currentLocation == null
        ? Container(
            child: Center(
              child: Text(
                'Cargando mapa..',
                style: TextStyle(
                    fontFamily: 'Avenir-Medium', color: Colors.grey[400]),
              ),
            ),
          )
        : Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: GoogleMap(
              onMapCreated: onMapCreated,
              polylines: polyline,
              initialCameraPosition: CameraPosition(
                  target: LatLng(
                      currentLocation.latitude, currentLocation.longitude),
                  zoom: 12.0),
              mapType: MapType.normal,
              myLocationEnabled: true,
              zoomControlsEnabled: false,
              myLocationButtonEnabled: false,
              markers: Set.of((_markers != null) ? _markers : []),
            ),
          );
  }

  Widget _buildContainer() {
    return Align(
      alignment: Alignment.topLeft,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 20.0),
        width: MediaQuery.of(context).size.width * 0.9,
        height: 150.0,
        //width: MediaQuery.of(context).size.width,
        child: ListView(
          //scrollDirection: Axis.horizontal,
          children: <Widget>[
            SizedBox(width: 10.0),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _boxes(),
            ),
            SizedBox(width: 10.0),
          ],
        ),
      ),
    );
  }

  Widget _boxes() {
    return GestureDetector(
      onTap: ()=>{
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => VerAlertaDetalle(alerta: widget.alerta))),
      },
      child: Container(
        child: new FittedBox(
          child: Material(
              color: Colors.white,
              elevation: 14.0,
              borderRadius: BorderRadius.circular(24.0),
              shadowColor: Color(0x802196F3),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.all(15.0),
                    width: 70,
                    height: 70,
                    child: new CircleAvatar(
                      backgroundColor: Color(0xffF67272),
                      child: ClipOval(
                        child: Image(
                          image: AssetImage('assets/notificacion.png'),
                          fit: BoxFit.fill,
                          width: 50,
                          height: 50,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: myDetailsContainer1(),
                    ),
                  ),
                ],
              )),
        ),
      ),
    );
  }

  Widget myDetailsContainer1() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        FutureBuilder(
            future: buscarHora(widget.alerta.createdAt),
            builder: (context, AsyncSnapshot<String> snap) {
              if (!snap.hasData) {
                return Text("");
              } else {
                return Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(right: 8.0, top: 8.0),
                      child: Text(
                        snap.data,
                        style:
                            TextStyle(color: Color(0xffB0A7A7), fontSize: 18.0),
                      ),
                    ),
                  ],
                );
              }
            }),
        SizedBox(height: 5.0),
        Row(
          children: [
            Container(
              child: Text(
                widget.alerta.nombre_alerta,
                style: TextStyle(
                    color: Color(0xffF67272),
                    fontSize: 26.0,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        SizedBox(height: 5.0),
        FutureBuilder(
            future: buscarFecha(widget.alerta.createdAt),
            builder: (context, AsyncSnapshot<String> snap) {
              if (!snap.hasData) {
                return Text("");
              } else {
                return Row(
                  children: [
                    Container(
                      child: Text(snap.data,
                        style:
                            TextStyle(color: Color(0xffB0A7A7), fontSize: 18.0),
                      ),
                    )
                  ],
                );
              }
            }),
        SizedBox(height: 10.0),
        FutureBuilder(
            future: _cargarDireccion(double.parse(widget.alerta.longitud),
                double.parse(widget.alerta.latitud)),
            builder: (context, AsyncSnapshot<String> snap) {
              if (!snap.hasData) {
                return Text("");
              } else {
                return Container(
                  padding: EdgeInsets.only(top: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      FaIcon(
                        FontAwesomeIcons.mapMarkerAlt,
                        color: Color(0xffF67272),
                        size: 24,
                      ),
                      SizedBox(width: 5.0),
                      Expanded(
                        child: Text(
                          "Situación de emergencia en Irene Morales 025",
                          style: TextStyle(
                            color: Colors.black54,
                            fontSize: 24.0,
                          ),
                        ),
                      )
                    ],
                  ),
                );
              }
            }),
      ],
    );
  }

   void cargarLocalizacion() async {
    LocationData _location;
    try {
      _location = await location.getLocation();

      error = null;
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        error = 'Permission denied';
      } else if (e.code == "PERMISSION_DENIED_NEVER_ASK") {
        error = 'Permission denied';
      }
      _location = null;
    }

    setState(() {
      currentLocation = _location;
      _markers.add(Marker(
        markerId: MarkerId('MiPosicion'),
        position: LatLng(currentLocation.latitude, currentLocation.longitude),
        infoWindow: InfoWindow(title: 'Mi Posición'),
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
      ));

      _markers.add(Marker(
        markerId: MarkerId('PosicionVictima'),
        position: posicionVictima,
        infoWindow: InfoWindow(title: 'Posicion Destino'),
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
      ));
      getaddressPoints();
    });
  }

  Future<String> buscarFecha(fechaData) async {
    var fecha = DateFormat.yMMMMd('es_ES').format(fechaData);
    return fecha;
  }

  Future<String> buscarHora(fechaData) async {
    var hora = DateFormat.jm().format(fechaData);
    return hora;
  }
  Future<String> _cargarDireccion(longitud, latitud) async {
    String url =
        'https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitud},${longitud}&key=AIzaSyDCrbMhvIppUjkm0kO9MF6KoqNia9fJlgc';
    var response = await http.get(url);
    var data = jsonDecode(response.body);
    if (response.statusCode == 200) {
      return data['results'][0]['formatted_address'].toString();
    } else {
      return "Error";
    }
  }

  getaddressPoints() async {
    print('asdasdasd');
    print(currentLocation);
    print('asdasdasd!!!');
    routeCoords = await googleMapPolyline.getCoordinatesWithLocation(
        origin: LatLng(currentLocation.latitude, currentLocation.longitude),
        destination: posicionVictima,
        mode: RouteMode.driving);
  }

  void onMapCreated(GoogleMapController controller) {
    setState(() {
      _controller = controller;
      _centerView();
      polyline.add(Polyline(
          polylineId: PolylineId('route1'),
          visible: true,
          points: routeCoords,
          width: 4,
          color: Colors.blue,
          startCap: Cap.roundCap,
          endCap: Cap.buttCap));
    });
  }

  _currentLocation() async {
    await _controller.getVisibleRegion();
    var camaraPosition = CameraPosition(
      bearing: 0,
      target: LatLng(currentLocation.latitude, currentLocation.longitude),
      zoom: 17.0,
    );
    var cameraUpdate = CameraUpdate.newCameraPosition(camaraPosition);
    _controller.animateCamera(cameraUpdate);
  }

  _centerView() async {
    await _controller.getVisibleRegion();

    var left = min(currentLocation.latitude, posicionVictima.latitude);
    var right = max(currentLocation.latitude, posicionVictima.latitude);
    var top = max(currentLocation.longitude, posicionVictima.longitude);
    var bottom = min(currentLocation.longitude, posicionVictima.longitude);

    var bounds = LatLngBounds(
      southwest: LatLng(left, bottom),
      northeast: LatLng(right, top),
    );
    var cameraUpdate = CameraUpdate.newLatLngBounds(bounds, 50);
    _controller.animateCamera(cameraUpdate);
  }
}
