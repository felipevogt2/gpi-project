import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app/controller/api.dart';
import 'package:flutter_app/pages/ver_alerta_detalle.dart';
import 'package:flutter_app/routes.dart';
import 'package:flutter_app/routes.dart';
import 'package:flutter_app/pages/login.dart';
import 'package:flutter_app/pages/emergencias.dart';
import 'package:flutter_app/pages/formulario_alerta.dart';
import 'package:flutter_app/pages/inicio.dart';
import 'package:flutter_app/pages/ver_alerta.dart';
import 'package:flutter_app/pages/login_funcionario.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_app/model/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NavDrawer extends StatefulWidget {
  NavDrawer({Key key, @required this.user}) : super(key: key);
  final User user;
  @override
  State<StatefulWidget> createState() => DrawerState();
}

class DrawerState extends State<NavDrawer> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Drawer(
        child: ListView(
          //padding: EdgeInsets.zero,
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountEmail: Text(widget.user.email),
              accountName: Text(widget.user.nombres),
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage("https://i.ibb.co/v3ytJZy/man.png"),
              ),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      colorFilter: new ColorFilter.mode(
                          Colors.black.withOpacity(0.8), BlendMode.dstATop),
                      image: NetworkImage("https://i.ibb.co/3SkMc0f/luz.jpg"),
                      fit: BoxFit.fill)),
            ),
            Container(
              padding: EdgeInsets.all(8.0),
              child: Column(
                children: [
                  new Divider(),
                  ListTile(
                      leading: FaIcon(
                        FontAwesomeIcons.bell,
                        color: Color(0xff837E7E),
                      ),
                      title: Text(
                        'Ver emergencias',
                        style: TextStyle(color: Color(0xff837E7E), fontSize: 16),
                      ),
                      onTap: () => {
                        Navigator.push(context,MaterialPageRoute(builder: (context) => Emergencias())),
                      }),
                  new Divider(),
                  ListTile(
                    leading: Icon(
                      Icons.exit_to_app,
                      color: Color(0xff837E7E),
                    ),
                    title: Text(
                      'Salir',
                      style: TextStyle(color: Color(0xff837E7E), fontSize: 16),
                    ),
                    onTap: () => {
                      logout(),
                    },
                  ),
                  new Divider(),
                ],
              ),
            ),
          ],
        ));
  }

  void borrarDatos() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.remove('user');
    localStorage.remove('token');
    debugPrint("user y token borrados del almacenamiento");

    Navigator.push(context, MaterialPageRoute(builder: (context) => LoginFuncionario()));
  }

// Future builder
  void logout() async {
    var res = await Network().getData('v1/auth/logout');
    var body = json.decode(res.body);
    print(json.decode(res.body));
    if (res.statusCode == 200) {
      borrarDatos();
    } else {
      debugPrint('No se puedo deslogear');
    }
  }

}
