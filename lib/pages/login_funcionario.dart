import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:convert';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_app/pages/emergencias.dart';
import 'package:flutter_app/controller/api.dart';
import 'package:shared_preferences/shared_preferences.dart';
class LoginFuncionario extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LoginFuncionarioState();
}

class LoginFuncionarioState extends State<LoginFuncionario> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;
  String _correo;
  String _password;
  bool _obscureText = true;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Login'),
        backgroundColor: Color(0XFFF67272),
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height * 0.9,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(35.0),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/fondoLogin.png"),
                  fit: BoxFit.cover,
                ),
              ),
              child: Container(
                padding: EdgeInsets.all(15.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: Column(
                  children: [
                    Flexible(
                      child: _formWidget(),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _formWidget() {
    return Form(
      key: _formKey,
      child: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(
                top: 20, bottom: 20.0, left: 10, right: 10),
            child: Image.asset('assets/logo.png'),
          ),
          SizedBox(
            height: 20,
          ),
          TextFormField(
              autocorrect: false,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Correo electronico'),
              validator: (val) {
                if (val.isEmpty) {
                  return 'Porfavor ingrese un correo';
                } else {
                  return null;
                }
              },
              onChanged: (val) {
                _correo = val;
              }),
          SizedBox(
            height: 20,
          ),
          TextFormField(
              obscureText: _obscureText,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Contraseña',
                suffixIcon: IconButton(
                  icon: FaIcon(_obscureText
                      ? FontAwesomeIcons.eyeSlash
                      : FontAwesomeIcons.eye,color: Colors.grey,),
                  onPressed: _toggle,
                ),
              ),
              validator: (val) {
                if (val.isEmpty) {
                  return 'Porfavor ingrese una contraseña';
                } else {
                  return null;
                }
              },
              onChanged: (val) {
                _password = val;
              }),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              InkWell(
                  child: new Text(
                    '¿Olvidaste tu clave?',
                    style: TextStyle(color: Color(0XFFF67272)),
                  ),
                  onTap: () =>
                      launch('https://comisariavirtual.cl/password/reset')),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0),
            child: RaisedButton(
                onPressed: () {
                  _login();
                },
                padding: EdgeInsets.only(top: 10, bottom: 10),
                color: Color(0XFFF67272),
                child: Text('Entrar',
                    style: TextStyle(fontSize: 20, color: Colors.white)),
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(10.0))),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text("¿No tienes cuenta?"),
              InkWell(
                  child: new Text(
                    'Consige tu cuenta',
                    style: TextStyle(color: Color(0XFFF67272)),
                  ),
                  onTap: () => launch('https://comisariavirtual.cl/login')),
            ],
          ),
        ],
      ),
    );
  }

  _login() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    print(_correo);
    print(_password);
    Map data = {'email': _correo, 'password': _password};

    var jsonResponse = null;
    var response = await Network().authData(data, 'v1/auth/login');
    if (response.statusCode == 200) {
      jsonResponse = json.decode(response.body);
      print(jsonResponse.toString());
      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
        });
        localStorage.setString("token", json.encode(jsonResponse['token']));
        localStorage.setString('user', json.encode(jsonResponse['user']));
        Navigator.push(
          context,
          new MaterialPageRoute(builder: (context) => Emergencias()),
        );
      }
    } else {
      setState(() {
        _isLoading = false;
      });
      print(jsonResponse);
      _mensajeNoAutentificado();
    }
  }

  Future<void> _mensajeNoAutentificado() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Usuario no autentificado'),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    'Correo electronico y/o contraseña sin coincidencias. Intentelo nuevamente')
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok', style: TextStyle(color: Color(0XFFF67272))),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
