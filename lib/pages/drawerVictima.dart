import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app/pages/inicio.dart';
import 'package:flutter_app/pages/principal.dart';
import 'package:flutter_app/pages/login_funcionario.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';


class NavDrawerVictima extends StatefulWidget {
  NavDrawerVictima({Key key, @required this.nombre,@required this.rut}) : super(key: key);
  final String nombre;
  final String rut;
  @override
  State<StatefulWidget> createState() => DrawerVictimaState();
}

class DrawerVictimaState extends State<NavDrawerVictima> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Drawer(
        child: ListView(
          //padding: EdgeInsets.zero,
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountEmail: Text(widget.nombre),
              accountName: Text(widget.rut),
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage("https://i.ibb.co/v3ytJZy/man.png"),
              ),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      colorFilter: new ColorFilter.mode(
                          Colors.black.withOpacity(0.8), BlendMode.dstATop),
                      image: NetworkImage("https://i.ibb.co/3SkMc0f/luz.jpg"),
                      fit: BoxFit.fill)),
            ),
            Container(
              padding: EdgeInsets.all(8.0),
              child: Column(
                children: [
                  new Divider(),
                  ListTile(
                      leading: FaIcon(
                        FontAwesomeIcons.bell,
                        color: Color(0xff837E7E),
                      ),
                      title: Text(
                        'Inicio',
                        style: TextStyle(color: Color(0xff837E7E), fontSize: 16),
                      ),
                      onTap: () => {
                        Navigator.push(context,MaterialPageRoute(builder: (context) => Inicio())),
                      }),
                  new Divider(),
                  ListTile(
                    leading: Icon(
                      Icons.exit_to_app,
                      color: Color(0xff837E7E),
                    ),
                    title: Text(
                      'Salir',
                      style: TextStyle(color: Color(0xff837E7E), fontSize: 16),
                    ),
                    onTap: () => {
                      logout(),
                    },
                  ),
                  new Divider(),
                ],
              ),
            ),
          ],
        ));
  }

  void borrarDatos() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.remove('userData');
    localStorage.remove('tokenClaveUnica');
    debugPrint("user y token borrados del almacenamiento");

    Navigator.push(context, MaterialPageRoute(builder: (context) => LoginFuncionario()));
  }

// Future builder
  void logout() async {
    Navigator.push(context, MaterialPageRoute(builder: (context) => PrincipalPage()));
  }

}