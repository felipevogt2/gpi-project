import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/pages/drawer.dart';
import 'package:flutter_app/model/user.dart';
import 'package:flutter_app/model/alerta.dart';
import 'dart:convert';
import 'package:flutter_app/controller/api.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_app/pages/ver_alerta.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

class Emergencias extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => EmergenciasState();
}

class EmergenciasState extends State<Emergencias> {
  User userData;
  List<Alerta> alertasData;
  String hola;

  @override
  void initState() {
    super.initState();
    initializeDateFormatting();
    getUserData();
    _cargarDireccion(-38.768681, -72.747652);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Emergencias"),
        backgroundColor: Color(0xffF67272),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.mail,
              color: Colors.white,
            ),
            onPressed: () {
              // do something
            },
          )
        ],
      ),
      drawer: userData != null ? NavDrawer(user: userData) : Container(),
      body: alertasData != null
          ? Container(
              child: ListView.builder(
                itemCount: alertasData.length,
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) => Container(
                  width: MediaQuery.of(context).size.width,
                  padding:
                      EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                  child: _boxes(alertasData[index]),
                ),
              ),
            )
          : Center(
              child: Text("No existen emergencias registradas"),
            ),
    );
  }

  Widget _boxes(Alerta alerta) {
    return GestureDetector(
        onTap: () => {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => VerAlerta(alerta: alerta))),
            },
        child: Card(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(24.0),
          ),
          child: ClipPath(
            child: Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
              decoration: BoxDecoration(
                  border: Border(
                      left: BorderSide(
                          color: Color(int.parse('0xff' + alerta.color)),
                          width: 20))),
              child: detalleEmergencia(alerta),
            ),
            clipper: ShapeBorderClipper(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20))),
          ),
        ));
  }

  Widget detalleEmergencia(Alerta alerta) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        FutureBuilder(
            future: (_cambiarFecha(alerta.createdAt)),
            builder: (context, AsyncSnapshot<String> snap) {
              if (!snap.hasData) {
                return Text("");
              } else {
                return Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      snap.data,
                      style:
                          TextStyle(color: Color(0xffB0A7A7), fontSize: 16.0),
                    ),
                  ],
                );
              }
            }),
        SizedBox(
          height: 5.0,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(
              alerta.nombre_alerta,
              style: TextStyle(
                  color: Colors.grey,
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
        SizedBox(
          height: 5.0,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(
              "Victima ${alerta.nombre_victima}",
              style: TextStyle(color: Colors.black54, fontSize: 18.0),
            )
          ],
        ),
        SizedBox(
          height: 5.0,
        ),
        SizedBox(
          height: 5.0,
        ),
        FutureBuilder(
            future: _cargarDireccion(
                double.parse(alerta.longitud), double.parse(alerta.latitud)),
            builder: (context, AsyncSnapshot<String> snap) {
              if (!snap.hasData) {
                return Text("");
              } else {
                return Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Flexible(
                      child: Text(
                        snap.data,
                        style: TextStyle(
                          color: Colors.black54,
                          fontSize: 18.0,
                        ),
                      ),
                    ),
                  ],
                );
              }
            }),
      ],
    );
  }

  Widget _inputSearch() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
      padding: EdgeInsets.only(right: 8.0, left: 8.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.1),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 2), // changes position of shadow
          ),
        ],
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 1,
            child: TextFormField(
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Buscar por fecha',
                hintStyle: TextStyle(color: Colors.grey),
                icon: Icon(Icons.search, color: Colors.grey),
              ),
            ),
          ),
          Expanded(
            flex: 0,
            child: Row(
              children: [
                IconButton(
                    onPressed: () {},
                    icon: Icon(
                      Icons.more_vert,
                      color: Colors.grey,
                    )),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<String> _cargarDireccion(longitud, latitud) async {
    var direccion;
    String url =
        'https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitud},${longitud}&key=AIzaSyDCrbMhvIppUjkm0kO9MF6KoqNia9fJlgc';
    var response = await http.get(url);
    var data = jsonDecode(response.body);
    if (response.statusCode == 200) {
      return data['results'][0]['formatted_address'].toString();
    } else {
      return "Error";
    }
  }

  Future<String> _cambiarFecha(fechaData) async {
    var fecha = DateFormat.yMMMMd('es_ES').format(fechaData);
    var hora= DateFormat.jm().format(fechaData);
    return fecha+' - '+hora;
  }

  _cargarEmergencias(id) async {
    var response = await Network().getData('alertaFuncionario/${id}');
    var jsonEmergencias = null;

    if (response.statusCode == 200) {
      jsonEmergencias = json.decode(response.body);
      print(jsonEmergencias);
      var list = jsonEmergencias as List;
      setState(() {
        alertasData = list.map((i) => Alerta.fromJson(i)).toList();
      });
    } else {
      setState(() {
        alertasData = null;
      });
    }
  }

  void getUserData() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var user = jsonDecode(localStorage.getString('user'));
    if (user != null) {
      setState(() {
        userData = User.fromJson(user);
        _cargarEmergencias(userData.id);
      });
    }
  }
}
