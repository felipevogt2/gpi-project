import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/model/alerta.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_app/controller/api.dart';
import 'package:shared_preferences/shared_preferences.dart';
class VerAlertaDetalle extends StatefulWidget {
  VerAlertaDetalle({Key key, @required this.alerta}) : super(key: key);
  final Alerta alerta;

  @override
  State<StatefulWidget> createState() => VerAlertaDetalleState();
}

class VerAlertaDetalleState extends State<VerAlertaDetalle> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Alerta ${widget.alerta.id_alerta}'),
          backgroundColor: Color(0xffF67272),
          actions: <Widget>[],
        ),
        body: ListView(
          children: [
          Container(
              height: 800,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(top:10),
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
            child:   Container(
              child: _contenido(),

          )
          )]),);
  }

  Widget _contenido() {
    return Card(
        elevation: 10.0,
        child: Container(
          child: Padding(
            padding: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Container(
                      margin: const EdgeInsets.all(15.0),
                      width: 60,
                      height: 60,
                      child: new CircleAvatar(
                        backgroundColor: Colors.red,
                        child: ClipOval(
                          child: Image(
                            image: AssetImage('assets/notificacion.png'),
                            fit: BoxFit.fill,
                            width: 40,
                            height: 40,
                          ),
                        ),
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Detalle de incidente",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20)),
                        Row(
                          children: [
                            Text("Tipo: ",
                                style: TextStyle(
                                    color: Colors.black54,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16)),
                            Text(widget.alerta.nombre_categoria,
                                style: TextStyle(
                                    color: Colors.black54, fontSize: 16)),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        IconButton(
                          icon: Icon(
                            Icons.mail,
                            color: Colors.black26,
                            size: 28,
                          ),
                          onPressed: () {
                            // do something
                          },
                        ),
                        Text("")
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 30),
                Row(
                  children: [
                    Text("Fecha de incidente:",
                        style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black54, fontSize: 16)),
                  ],
                ),
                FutureBuilder(
                    future: buscarFecha(widget.alerta.createdAt),
                    builder: (context, AsyncSnapshot<String> snap) {
                      if (!snap.hasData) {
                        return Text("");
                      } else {
                        return Row(
                          children: [
                            Text(snap.data,
                                style: TextStyle(
                                    color: Colors.black54, fontSize: 16)),
                          ],
                        );
                      }
                    }),
                SizedBox(height: 10),
                Row(
                  children: [
                    Text("Hora de incidente:",
                        style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black54, fontSize: 16)),
                  ],
                ),
                FutureBuilder(
                    future: buscarHora(widget.alerta.createdAt),
                    builder: (context, AsyncSnapshot<String> snap) {
                      if (!snap.hasData) {
                        return Text("");
                      } else {
                        return Row(
                          children: [
                            Text(snap.data,
                                style: TextStyle(
                                    color: Colors.black54, fontSize: 16)),
                          ],
                        );
                      }
                    }),
                SizedBox(height: 10),
                Row(
                  children: [
                    Text("Dirección de suceso:",
                        style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black54, fontSize: 16)),
                  ],
                ),
                FutureBuilder(
                    future: _cargarDireccion(double.parse(widget.alerta.latitud),
                        double.parse(widget.alerta.longitud)),
                    builder: (context, AsyncSnapshot<String> snap) {
                      if (!snap.hasData) {
                        return Text("");
                      } else {
                        return Row(
                          children: [
                            Flexible(
                              child: Text(snap.data,
                                  style: TextStyle(
                                      color: Colors.black54, fontSize: 16)),
                            )
                          ],
                        );
                      }
                    }),
                SizedBox(height: 10),
                Row(
                  children: [
                    Text("Categoria de emergencia",
                        style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black54, fontSize: 16)),
                  ],
                ),
                Row(
                  children: [
                    Text(widget.alerta.nombre_alerta,
                        style: TextStyle(color: Colors.black54, fontSize: 16)),
                  ],
                ),
                SizedBox(height: 10),
                Row(
                  children: [
                    Text("Tipo Victima:",
                        style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black54, fontSize: 16)),
                  ],
                ),
                Row(
                  children: [
                    Text(widget.alerta.tipo_victima,
                        style: TextStyle(color: Colors.black54, fontSize: 16)),
                  ],
                ),
                SizedBox(height: 10),
                Row(
                  children: [
                    Text("Victima:",
                        style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black54, fontSize: 16)),
                  ],
                ),
                Row(
                  children: [
                    Text(widget.alerta.nombre_victima,
                        style: TextStyle(color: Colors.black54, fontSize: 16)),
                  ],
                ),
                SizedBox(height: 10),
                Row(
                  children: [
                    Text("Vinculo Atacante:",
                        style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black54, fontSize: 16)),
                  ],
                ),
                Row(
                  children: [
                    Text(widget.alerta.vinculo_atacante,
                        style: TextStyle(color: Colors.black54, fontSize: 16)),
                  ],
                ),
                SizedBox(height: 10),
                Row(
                  children: [
                    Text("Nombre Atacante:",
                        style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black54, fontSize: 16)),
                  ],
                ),
                Row(
                  children: [
                    Text(widget.alerta.nombre_atacante,
                        style: TextStyle(color: Colors.black54, fontSize: 16)),
                  ],
                ),
                SizedBox(height: 10),
                Row(
                  children: [
                    Text("Rut Alertante: ",
                        style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black54, fontSize: 16)),
                  ],
                ),
                Row(
                  children: [
                    Text(widget.alerta.rut_alertante,
                        style: TextStyle(color: Colors.black54, fontSize: 16)),
                  ],
                ),
                SizedBox(height: 10),
                Row(
                  children: [
                    Text("Comisaria Asignada:",
                        style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black54, fontSize: 16)),
                  ],
                ),
                Row(
                  children: [
                    Text(widget.alerta.nombre,
                        style: TextStyle(color: Colors.black54, fontSize: 16)),
                  ],
                ),
                SizedBox(height: 10),
                Row(
                  children: [
                    Text("Descripcion del suceso: ",
                        style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black54, fontSize: 16)),
                  ],
                ),
                Row(
                  children: [
                    Flexible(
                      child: Text(widget.alerta.descripcion,
                          style: TextStyle(color: Colors.black54, fontSize: 16)),
                    )
                  ],
                ),
                Spacer(),
                widget.alerta.asignada != "no" ? Container():
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FlatButton(
                      color: Color(0xffF67272),
                      textColor: Colors.white,
                      disabledColor: Colors.grey,
                      disabledTextColor: Colors.black,
                      padding: EdgeInsets.all(15),
                      splashColor: Colors.blueAccent,
                      onPressed: () {
                        _asignarFuncionario();
                      },
                      child: Text(
                        "Asignar",
                        style: TextStyle(fontSize: 20.0),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        )
    );
  }

  _asignarFuncionario() async {
    print("entrando");
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var user = json.decode(localStorage.getString("user"));
    Map data = {'id_alerta': widget.alerta.id_alerta, 'id_user':user['id']};
    var response = await Network().authData(data,'asignarAlertaFuncionario');
    var jsonEmergencias = null;

    if (response.statusCode == 200) {
      print("entrando 2");
        setState(() {
          widget.alerta.asignada="si";
        });
        _mensajeIngresado();

    } else {
      setState(() {
        widget.alerta.asignada="no";
      });
      _mensajeNoIngresado();
    }
  }
  Future<void> _mensajeIngresado() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Alerta Asignada con exito'),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    'La alerta se le fue asignada con total exito')
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok', style: TextStyle(color: Color(0XFFF67272))),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
  Future<void> _mensajeNoIngresado() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Error'),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    'La alerta no se pudo asignar')
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok', style: TextStyle(color: Color(0XFFF67272))),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
  Future<String> buscarFecha(fechaData) async {
    var fecha = DateFormat.yMMMMd('es_ES').format(fechaData);
    return fecha;
  }

  Future<String> buscarHora(fechaData) async {
    var hora = DateFormat.jm().format(fechaData);
    return hora;
  }

  Future<String> _cargarDireccion(latitud, longitud) async {
    String url =
        'https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitud},${longitud}&key=AIzaSyDCrbMhvIppUjkm0kO9MF6KoqNia9fJlgc';
    var response = await http.get(url);
    var data = jsonDecode(response.body);
    if (response.statusCode == 200) {
      return data['results'][0]['formatted_address'].toString();
    } else {
      return "Error";
    }
  }
}
