import 'dart:convert';
import 'package:flutter_app/pages/drawerVictima.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/pages/formulario_alerta.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:location/location.dart';

class Inicio extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => InicioState();
}

class InicioState extends State<Inicio> {
  var categorias = [];
  String rut_alertante;
  String nombre_victima;
  String direccion;
  String latitud;
  String longitud;
  Location location = new Location();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    getCategorias();
    getUserData();
  }

  void getCategorias() async {
    try {
      String url = 'http://34.232.65.48/api/verCategorias';
      var response = await http.get(url);
      var data = jsonDecode(response.body);

      if (response.statusCode == 200) {
        setState(() {
          categorias = data;
        });
      }
      print(data);
    } catch (e) {
      print(e);
    } finally {}
  }

  Future<void> getUserData() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var userData = jsonDecode(localStorage.getString('userData'));
    print(userData);
    if (userData != null) {
      setState(() {
        rut_alertante = userData['RolUnico']['numero'].toString() +
            "-" +
            userData['RolUnico']['DV'].toString();
        nombre_victima = userData['name']['nombres'][0].toString() +
            " " +
            userData['name']['apellidos'][0].toString();
        print(rut_alertante);
      });
    }
  }

  Future<void> geolocate() async {
    LocationData _location;
    _location = await location.getLocation();

    latitud = _location.latitude.toString();
    longitud = _location.longitude.toString();
    print(latitud + " " + longitud);
  }

  void sendAlerta() async {
    String url = 'http://34.232.65.48/api/agregarAlerta';
    print(nombre_victima);
    print(rut_alertante);
    print(latitud);
    print(longitud);
    var response = await http.post(url, headers: {
      'Connection': 'Keep-Alive'
    }, body: {
      'tipo_alerta_id': '6',
      'tipo_victima': 'Desconocido',
      'nombre_victima': nombre_victima,
      'nombre_atacante': 'Desconocido',
      'vinculo_atacante': 'Desconocido',
      'descripcion': 'Emergencia urgente',
      'rut_alertante': rut_alertante,
      'latitud': latitud,
      'longitud': longitud,
    });

    if (response.statusCode == 200) {
      _showMsg("Alarma enviada");
    } else {
      _showMsg("Ha ocurrido un  prboema...");
    }
    print(response.body);
  }

  _showMsg(msg) {
    final snackBar = SnackBar(
      content: Text(msg),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {
          // Some code to undo the change!
        },
      ),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  void alarmar() async {
    Future.wait<void>([getUserData(), geolocate()])
        .then((value) => {sendAlerta()});
  }

  Column getGrid(item) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        FormularioAlerta(item['id_categoria'])));
          },
          child: Container(
            margin: const EdgeInsets.all(15.0),
            width: 70,
            height: 70,
            child: new CircleAvatar(
              backgroundColor: Color(int.parse('0xff' + item['color'])),
              child: ClipOval(
                child: Image(
                  image: NetworkImage(
                      'http://34.232.65.48/storage/imagenesCategoria/${item['imagen']}'),
                  fit: BoxFit.fill,
                  width: 50,
                  height: 50,
                ),
              ),
            ),
          ),
        ),
        Text(item['nombre_categoria']),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: const Text('Alerta'),
        backgroundColor: Color(0xffF67272),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.mail,
              color: Colors.white,
            ),
            onPressed: () {
              // do something
            },
          )
        ],
      ),
      drawer: nombre_victima != null && rut_alertante != null
          ? NavDrawerVictima(
              rut: rut_alertante,
              nombre: nombre_victima,
            )
          : Container(),
      body: Container(
        child: Column(
          children: [
            SizedBox(height: 40),
            Center(
              child: FlatButton(
                color: Color(0xffFEC7C7),
                padding: EdgeInsets.all(20),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                textColor: Colors.white,
                child: RichText(
                  text: TextSpan(
                    children: [
                      WidgetSpan(
                        child: Image(
                          image: AssetImage('assets/notificacion.png'),
                          fit: BoxFit.fill,
                          width: 35,
                          height: 35,
                        ),
                      ),
                      TextSpan(
                          text: "Alarma YA!",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold)),
                    ],
                  ),
                ),
                onPressed: () {
                  _showMyDialog();
                },
              ),
            ),
            SizedBox(height: 40),
            Center(
                child: Container(
                    padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                    child: Text(
                      'Denuncia un tipo de incidente del cual eres testigo, victima o agresor',
                      style: TextStyle(fontSize: 20),
                    ))),
            SizedBox(height: 40),
            Expanded(
              child: GridView.count(
                // Create a grid with 2 columns. If you change the scrollDirection to
                // horizontal, this produces 2 rows.
                crossAxisCount: 3,
                // Generate 100 widgets that display their index in the List.
                children: List.generate(categorias.length, (index) {
                  return Center(
                    child: getGrid(categorias[index]),
                  );
                }),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Alerta YA!'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('¿Estas seguro de que quieres enviar una alerta?'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              textColor: Colors.red,
              child: Text('No'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Si'),
              onPressed: () {
                alarmar();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
