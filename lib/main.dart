import 'package:flutter/material.dart';
import 'package:flutter_app/routes.dart';
import 'package:flutter_app/pages/login_funcionario.dart';
import 'package:flutter_app/pages/principal.dart';
void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyApp> {
  Widget rootPage = LoginFuncionario();
  Widget rootPage2 = PrincipalPage();
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: rootPage2,
      routes: buildAppRoutes(),
    );
  }
}
