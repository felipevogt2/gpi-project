import 'package:flutter/material.dart';
import 'package:flutter_app/pages/ver_alerta.dart';
import 'package:flutter_app/pages/login.dart';
import 'package:flutter_app/pages/drawer.dart';
import 'package:flutter_app/pages/emergencias.dart';
Map<String, WidgetBuilder> buildAppRoutes() {
  return {
    '/verAlerta': (BuildContext context) => new VerAlerta(),
    '/login': (BuildContext context) => new Login(),
    '/barra': (BuildContext context) => new NavDrawer(),
    '/verEmergencia': (BuildContext context) => new Emergencias(),
  };
}
